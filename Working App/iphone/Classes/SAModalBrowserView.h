

#import <UIKit/UIKit.h>

@interface SAModalBrowserView : UIViewController <UIWebViewDelegate> {
    UIWebView *wv;
    
    @private
}

@property(nonatomic,strong) UIWebView *wv;


- (void)startWikiRequest;



@end
