
#import <Foundation/Foundation.h>

@interface Network : NSObject

+(BOOL) connectionAvailable;
+(void) showUnavailableMessage: (NSString*)message;

@end
