#ifndef __STARCROSS_TELESCOPE_H__
#define __STARCROSS_TELESCOPE_H__

#include <string>
#include <vector>
#include "stars/straight.h"

namespace starcross {
    
    struct Telescope {
        r3::Vec3f pos;
        std::string name;
    };
    
    void ReadTelescopeData( const std::string & filename );
    void ComputeTelescopePositions( std::vector<Telescope> & telescopes );
    
    // Get the rotation of the earth relative to ECI
    float GetCurrentEarthPhase();
    double GetJulianDate();
    
}




#endif // __STARCROSS_TELESCOPE_H__
