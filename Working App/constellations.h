

#ifndef __STARCROSS_CONSTELLATIONS_H__
#define __STARCROSS_CONSTELLATIONS_H__

#include "stars/straight.h"
#include <vector>
#include <string>

namespace starcross {
	
	struct Constellation {
		std::string name;
		std::vector< int > indexes; // pairs of hipNum indexes form a line segment
	};
	
	void ReadConstellations( const std::string & filename, std::vector<Constellation> & list );
	
}


#endif // __STARCROSS_CONSTELLATIONS_H__
