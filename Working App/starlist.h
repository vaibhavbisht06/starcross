#ifndef __STARCROSS_STARLIST_H__
#define __STARCROSS_STARLIST_H__

#include <string>
#include <vector>

namespace starcross {
	
	struct Star {
		int hipnum;
		float mag;
		float ra;
		float dec;
		float colorIndex;
		std::string name;
	};
	
	void ReadStarList( const std::string & filename, std::vector<Star> & list );
    void ReadGalaxyList( const std::string & filename, std::vector<Star> & list );
	
}

#endif //__STARCROSS_STARLIST_H__
