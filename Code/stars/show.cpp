#include "show.h"

#include "stars/commanding.h"
#include "stars/outcome.h"

#include <map>

using namespace std;
using namespace stars;

namespace {

	bool initialized = false;

	struct ModelDatabase {
		map< string, Model *> models;
	};
	ModelDatabase *modelDatabase;
	
	
	// listmodels command
	void ListModels( const vector< Token > & tokens ) {
		if ( modelDatabase == NULL ) {
			return;
		}
		map< string, Model * > &models = modelDatabase->models;
		for( map< string, Model * >::iterator it = models.begin(); it != models.end(); ++it ) {
			const string & n = it->first;
			//Model * m = it->second;
			Output( "%s", n.c_str() );					
		}
	}
	CommandFunc ListModelsCmd( "listmodels", "lists defined models", ListModels );
	
}

namespace stars {
	
	void InitModel() {
		if ( initialized ) {
			return;
		}
		modelDatabase = new ModelDatabase;
		
		initialized = true;
	}
	
	Model::Model( const string & mName ) 
	: name( mName )
	, vertexBuffer( NULL )
	, indexBuffer( NULL ) {
		assert( modelDatabase->models.count( name ) == 0 );
		modelDatabase->models[ name ];
	}

	Model::~Model() {
		modelDatabase->models.erase( name );
		delete vertexBuffer;
		delete indexBuffer;
	}
	
	VertexBuffer & Model::GetVertexBuffer() {
		if ( vertexBuffer == NULL ) {
			vertexBuffer = new VertexBuffer( name + "_vb" );
		}
		return *vertexBuffer;
	}
	IndexBuffer & Model::GetIndexBuffer() {
		if ( indexBuffer == NULL ) {
			indexBuffer = new IndexBuffer( name + "_ib" );
		}
		return *indexBuffer;
	}
	
	void Model::Draw() {
		vector< VertexBuffer * > vvb;
		vvb.push_back( vertexBuffer );
		
		stars::Draw( prim, vvb, indexBuffer );
	}
	
	
}


