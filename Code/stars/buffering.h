#ifndef __STARS_BUFFERING_H__
#define __STARS_BUFFERING_H__

#include <string>

namespace stars {
	
	void InitBuffer();
		
	class Buffer {
	protected:
		std::string name; 
		int size;
		unsigned int target;
		unsigned int obj;
		Buffer( const std::string & bufName, int bufTarget );
	public:
		~Buffer();
		
		const std::string & Name() {
			return name;
		}
		
		void Bind() const;
		void Unbind() const;

		int GetSize() const { return size; }

		void SetData( int size, const void * data );
		void SetSubdata( int offset, int size, const void * data );
		
		void GetData( void * data );
		void GetSubData( int offset, int size, void * data );
				
	};
	
	class VertexBuffer : public Buffer {
		int varying;
	public:
		VertexBuffer( const std::string & vbName );
		void SetVarying( int vbVarying );
		int GetVarying() const { return varying; }
		int GetNumVerts() const;
		
		void Enable() const;
		void Disable() const;
		
	};
	
	class IndexBuffer : public Buffer {
	public:
		IndexBuffer( const std::string & ibName );
	};
	
}

#endif // __STARS_BUFFERING_H__
