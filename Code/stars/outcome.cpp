#include "stars/outcome.h"
#include "stars/lay.h"
#include "stars/control.h"
#include "stars/threading.h"
#include "stars/variable.h"

#include <stdio.h>
#include <stdarg.h>

#if _WIN32
# include <Windows.h>
#endif
stars::VarBool win_OutputDebugString( "win_OutputDebugString", "Dump output to debugger in Windows.", stars::Var_Archive, false );

using namespace stars;
using namespace std;

namespace {
	Mutex outputMutex;
}

namespace stars {
	
	void InitOutput() {
	}
	
	void Output( const char *fmt, ... ) {
		ScopedMutex m( outputMutex );
		char str[16384];
		va_list args;
		va_start( args, fmt );
		r3Vsprintf( str, fmt, args );
		va_end( args );
		extern Console console;
		console.AppendOutput( str );
#if _WIN32
		if ( win_OutputDebugString.GetVal() ) {
			OutputDebugStringA( str );
			OutputDebugStringA( "\n" );
		}
#endif
		fprintf( stderr, "%s\n", str );
	}
	void OutputDebug( const char *fmt, ... ) {
		char str[1024];
		va_list args;
		va_start( args, fmt );
		r3Vsprintf( str, fmt, args );
		va_end( args );
		fprintf( stderr, "%s\n", str );
	}
	
}
