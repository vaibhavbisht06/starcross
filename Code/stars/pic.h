
#ifndef __STARS_PIC_H__
#define __STARS_PIC_H__

#include <string>

namespace stars {
	
	template< typename T >
	class Image {
	protected:
		int width;
		int height;
		int components;
	public:
		typedef T ComponentType; 
		virtual ~Image() {
		}
		virtual void *Data() = 0;
		
		int Width() const {
			return width;
		}
		
		int Height() const {
			return height;
		}
		
		int Components() const {
			return components;
		}
		
	};
	
	Image<unsigned char> * LoadStbImage( const std::string & filename, int desiredComponents = 0 );
	
}

#endif // __STARS_PIC_H__
