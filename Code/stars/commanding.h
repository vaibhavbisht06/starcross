#ifndef __STARS_COMMANDING_H__
#define __STARS_COMMANDING_H__

#include "stars/atomic.h"
#include "stars/parsing.h"
#include <string>

namespace stars {
	
	void InitCommand();
	
	class Command {
		Atom name;
		std::string helpText;
	public:
		Command( const char *cmdName, const char *cmdHelpText );
		virtual ~Command();
		Atom Name() const {
			return name;
		}
		std::string HelpText() const {
			return helpText;
		}
		virtual void Execute( const std::vector< Token > & tokens ) = 0;
	};
	
	Command * GetCommand( const char *cmdName );

	// CommandFunc calls a function
	class CommandFunc : public Command {
		void (*func)( const std::vector< Token > & );
	public:
		CommandFunc( const char *cmdName, const char *cmdHelpText, void( *cmdFunc )( const std::vector< Token > & ) ) 
		: Command( cmdName, cmdHelpText ), func( cmdFunc ) {
		}
		virtual void Execute( const std::vector< Token > & tokens ) {
			func( tokens );
		}
	};
	
	inline void CreateCommandFunc( const char *cmdName, const char *cmdHelpText, void (*cmdFunc)( const std::vector< Token > & ) ) {
		new CommandFunc( cmdName, cmdHelpText, cmdFunc );
	}
	

	// CommandObject calls a method of an object instance
	template <typename T>
	class CommandObject : public Command {
		T *obj;
		void (T::*method)( const std::vector< Token > & );
	public:
		CommandObject( const char *cmdName, const char *cmdHelpText, T * cmdObj, void( *cmdMethod )( const std::vector< Token > & ) ) 
		: Command( cmdName, cmdHelpText ), obj( cmdObj ), method( cmdMethod ) {
		}
		virtual void Execute( const std::vector< Token > & tokens ) {			
			obj->*method( tokens );
		}		
	};
	
	// helpers for creating and destroying CommandObject instances
	template <typename T>
	inline void CreateCommandObject( const char *cmdName, const char *cmdHelpText, T *cmdObj, void (T::*cmdMethod)( const std::vector< Token > & ) ) {
		new CommandObject<T>( cmdName, cmdHelpText, cmdObj, cmdMethod );
	}
	
	inline void DestroyCommand( const char *cmdName ) {
		delete GetCommand( cmdName );
	}
	
	void ExecuteCommand( const char * cmd );

}

#endif // __STARS_COMMANDING_H__
