#include "stars/buffering.h"
#include "stars/commanding.h"
#include "stars/drawing.h"
#include "stars/filing.h"
#include "stars/font.h"
#include "stars/gl.h"
#include "stars/show.h"
#include "stars/outcome.h"
#include "stars/texture.h"
#include "stars/variable.h"

using namespace std;

namespace stars {
	
	void Init( int argc, char **argv ) {
		vector< string > commands;
		string command;
		vector< string > av;
		for ( int i = 0; i < argc; i++ ) {
			av.push_back( string( argv[ i ] ) );
			if ( av.back()[0] ==  '+' ) {
				commands.push_back( command );
				av.back().erase( 0, 1 );
				command = av.back();
			} else {
				command += " ";
				command += av.back();
			}
		}
		commands.push_back( command );

		Output( "begin Command Line directives ----------------" );
		for ( int i = 1; i < (int)commands.size(); i++ ) {
			ExecuteCommand( commands[ i ].c_str() );
		}
		Output( "end   Command Line directives ----------------" );
		InitFilesystem();
		extern VarString f_basePath;
		if ( f_basePath.GetVal().size() == 0 ) {
			OutputDebug( "Unable to initialize f_basePath, exiting." );			
		} else {
			Output( "f_basePath = %s", f_basePath.GetVal().c_str() );
		}
		ExecuteCommand( "readbindings default" );
		ExecuteCommand( "readvars" );
#if _WIN32
		InitGLEntry();
#endif
		InitBuffer();
		InitDraw();
		InitFont();
		InitModel();
		InitTexture();
	}
}

