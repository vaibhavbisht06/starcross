#ifndef __STARS_VARIABLE_H__
#define __STARS_VARIABLE_H__

#include "stars/atomic.h"
#include "stars/straight.h"
#include <string>

namespace stars {
	
	void InitVar();
	
	enum VarFlags {
		Var_ReadOnly = 0x01,
		Var_Archive = 0x02
	};
	
	
	class Var {
		Atom name;
		std::string desc;
		int flags;
	public:
		Var( const char * varName, const char * varDesc, int varFlags );
		Atom Name() const {
			return name;
		}
		const std::string & Description() const {
			return desc;
		}
		int Flags() const {
			return flags;
		}
		virtual std::string Get() const = 0;
		virtual void Set( const char * str ) = 0;
		
		// These are opt-in methods that mostly only make sense for numerical types.
		virtual void Incr( const char * incrBy, const char * upperBound = NULL) {}
		virtual void Decr( const char * decrBy, const char * lowerBound = NULL) {}
	};

	class VarString : public Var {
		typedef std::string ValType;
		ValType val;
	public:
		VarString( const char * varName, const char * varDesc, int varFlags, const ValType & varVal ) 
		: Var( varName, varDesc, varFlags ), val( varVal ) {}
		virtual std::string Get() const;
		virtual void Set( const char * str );
		const ValType & GetVal() const { return val; }
		void SetVal( const ValType & varVal ) { val = varVal; }
	};
	
	class VarBool : public Var {
		typedef bool ValType;
		ValType val;
	public:
		VarBool( const char * varName, const char * varDesc, int varFlags, const ValType & varVal ) 
		: Var( varName, varDesc, varFlags ), val( varVal ) {}
		virtual std::string Get() const;
		virtual void Set( const char * str );
		const ValType & GetVal() const { return val; }
		void SetVal( const ValType & varVal ) { val = varVal; }
	};
	
	class VarInteger : public Var {
		typedef int ValType;
		ValType val;
	public:
		VarInteger( const char * varName, const char * varDesc, int varFlags, const ValType & varVal ) 
		: Var( varName, varDesc, varFlags ), val( varVal ) {}
		virtual std::string Get() const;
		virtual void Set( const char * str );
		const ValType & GetVal() const { return val; }
		void SetVal( const ValType & varVal ) { val = varVal; }
		virtual void Incr( const char * incrBy, const char * upperBound = NULL );
		virtual void Decr( const char * decrBy, const char * lowerBound = NULL );
	};
	
	class VarFloat : public Var {
		typedef float ValType;
		ValType val;
	public:
		VarFloat( const char * varName, const char * varDesc, int varFlags, const ValType & varVal ) 
		: Var( varName, varDesc, varFlags ), val( varVal ) {}
		virtual std::string Get() const;
		virtual void Set( const char * str );
		const ValType & GetVal() const { return val; }
		void SetVal( const ValType & varVal ) { val = varVal; }
		virtual void Incr( const char * incrBy, const char * upperBound = NULL );
		virtual void Decr( const char * decrBy, const char * lowerBound = NULL );
	};
	
	
	class VarVec2f : public Var {
		typedef Vec2f ValType;
		ValType val;
	public:
		VarVec2f( const char * varName, const char * varDesc, int varFlags, const ValType & varVal )
		: Var( varName, varDesc, varFlags ), val( varVal ) {}
		virtual std::string Get() const;
		virtual void Set( const char * str );
		const ValType & GetVal() const { return val; }
		void SetVal( const ValType & varVal ) { val = varVal; }
	};
	
	
	class VarVec3f : public Var {
		typedef Vec3f ValType;
		ValType val;
	public:
		VarVec3f( const char * varName, const char * varDesc, int varFlags, const ValType & varVal )
		: Var( varName, varDesc, varFlags ), val( varVal ) {}
		virtual std::string Get() const;
		virtual void Set( const char * str );
		const ValType & GetVal() const { return val; }
		void SetVal( const ValType & varVal ) { val = varVal; }
	};
	
	
	class VarVec4f : public Var {
		typedef Vec4f ValType;
		ValType val;
	public:
		VarVec4f( const char * varName, const char * varDesc, int varFlags, const ValType & varVal )
		: Var( varName, varDesc, varFlags ), val( varVal ) {}
		virtual std::string Get() const;
		virtual void Set( const char * str );
		const ValType & GetVal() const { return val; }
		void SetVal( const ValType & varVal ) { val = varVal; }
	};
	
	class VarRotationf : public Var {
		typedef Rotationf ValType;
		ValType val;
	public:
		VarRotationf( const char * varName, const char * varDesc, int varFlags, const ValType & varVal )
		: Var( varName, varDesc, varFlags ), val( varVal ) {}
		virtual std::string Get() const;
		virtual void Set( const char * str );
		const ValType & GetVal() const { return val; }
		void SetVal( const ValType & varVal ) { val = varVal; }
	};
	
	Var * FindVar( const char *varName );
	
}

#endif // __STARS_VARIABLE_H__
