#include "stars/threading.h"

using namespace stars;

namespace  {
	Mutex mainThreadMutex;
	int numThreads;
	
}

#ifdef __APPLE__
void *starsThreadStart( void *data ) 
#elif _WIN32
DWORD WINAPI r3ThreadStart( LPVOID data )
#endif
{
	Thread * thread = static_cast< Thread * > ( data );

	mainThreadMutex.Acquire();
	numThreads++;
	mainThreadMutex.Release();
	
	thread->Run();
	
	mainThreadMutex.Acquire();
	numThreads--;
	thread->running = false;
	mainThreadMutex.Release();
	return NULL;
}

namespace stars {
	
	void InitThread() {
	}
	
	void Thread::Start() {
		ScopedMutex m( mainThreadMutex );
		if ( running ) {
			return;
		}
		running = true;
#ifdef __APPLE__
		pthread_create( &threadId, NULL, r3ThreadStart, this );
#elif _WIN32
		threadId = CreateThread( NULL, 0, r3ThreadStart, this, 0, NULL );
#endif
	}
	
	
}

