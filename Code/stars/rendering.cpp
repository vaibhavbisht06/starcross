#include "stars/rendering.h"

#include "stars/gl.h"
#include <assert.h>

namespace {

	GLenum GlRenderBufferFormat[] = {
#if ! TARGET_OS_IPHONE
		0,                          // TextureFormat_INVALID,
		0,                          // TextureFormat_L,
		0,                          // TextureFormat_LA,
		GL_RGB8,                    // TextureFormat_RGB,
		GL_RGBA8,                   // TextureFormat_RGBA,
		GL_DEPTH_COMPONENT16,		// TextureFormat_DepthComponent,
		0                           // TextureFormat_MAX	
#else
		0,                          // TextureFormat_INVALID,
		0,                          // TextureFormat_L,
		0,                          // TextureFormat_LA,
		GL_RGB8_OES,                // TextureFormat_RGB,
		GL_RGBA8_OES,               // TextureFormat_RGBA,
		GL_DEPTH_COMPONENT16_OES,	// TextureFormat_DepthComponent,
		0                           // TextureFormat_MAX
#endif
	};	
	
}


namespace stars {
	
	RenderBuffer::RenderBuffer( TextureFormatEnum rbFormat, int rbWidth, int rbHeight ) 
	: format( rbFormat )
	, width( rbWidth )
	, height( rbHeight ) {
		glGenRenderbuffersEXT( 1, & obj );
		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GlRenderBufferFormat[ format ], width, height);
	}
	
	RenderBuffer::~RenderBuffer() {
		glDeleteRenderbuffersEXT( 1, & obj );
	}
	
	RenderBuffer * RenderBuffer::Create( TextureFormatEnum format, int width, int height ) {
		return new RenderBuffer( format, width, height );
	}
	
	void RenderBuffer::Bind() {
		glBindRenderbufferEXT( GL_RENDERBUFFER_EXT, obj );
	}
	
	void RenderBuffer::Unbind() {
		glBindRenderbufferEXT( GL_RENDERBUFFER_EXT, 0 );
	}
	
}


