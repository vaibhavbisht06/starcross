#ifndef __STARS_INITIAL_H__
#define __STARS_INITIAL_H__

// use X keysym defs
#define XK_MISCELLANY 1
#define XK_LATIN1 1
#include "stars/keysymdef.h"
#include "stars/staright.h"
#include <algorithm>

namespace stars {
	
	void InitInput();

	enum KeyStateEnum {
		KeyState_Down,
		KeyState_Up
	};
	
	typedef void (*KeyEventHandler)( int, KeyStateEnum );
	
	// returns the old one so you can chain / restore; pass NULL for default handler
	KeyEventHandler SetKeyHandler( KeyEventHandler handler );
	void ProcessKeyEvent( int key, KeyStateEnum keyState );

	int AsciiToKey( unsigned char key );
	
	
	struct Trackball {
		Trackball() {}
		Trackball( int w, int h ) : width( w ), height( h ) {}

		int width;
		int height;
		
		Rotationf GetRotation( Vec2f p0, Vec2f p1 ) {
			Vec2f dp = p1 - p0;
			if( dp.x == 0 && dp.y == 0) {
				return Rotationf();
			}
			
			float minDim = (float)std::min( width, height );
			minDim /= 2.f;
			Vec3f offset( width / 2.f, height / 2.f, 0);
			Vec3f a = ( Vec3f( p0.x, p0.y, 0) - offset ) / minDim;
			Vec3f b = ( Vec3f( p1.x, p1.y, 0) - offset ) / minDim;
			
			a.z = -(float)pow(2, -0.5 * a.Length() );
			a.Normalize();
			b.z = -(float)pow(2, -0.5 * b.Length() );
			b.Normalize();
			
			Vec3f axis = a.Cross( b );
			axis.Normalize();
			
			float angle = acos( a.Dot( b ) );
			return Rotationf( axis, angle );			
		}
		
		
	};
	
}

#endif // __STARS_INITIAL_H__
