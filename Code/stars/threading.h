#ifndef __STARS_THREADING_H__
#define __STARS_THREADING_H__

#ifdef __APPLE__
# include <pthread.h>
#elif _WIN32
# include <windows.h>
#endif



namespace stars {
	
	void InitThread();
	
#ifdef __APPLE__
	class Mutex {
		pthread_mutex_t mutex;
	public:
		Mutex() {
			pthread_mutex_init( &mutex, NULL );
		}
		void Acquire() {
			pthread_mutex_lock( &mutex );
		}
		void Release() {
			pthread_mutex_unlock( &mutex );
		}
	};
	
	class Threading {
		pthread_t threadId;
	public:
		Threading() : running( false ) {
		}
		bool running;
		void Start();
		virtual void Run() = 0;
	};
#elif _WIN32
	class Mutex {
		HANDLE mutex;
	public:
		Mutex() {
			mutex = CreateMutex( 0, FALSE, 0 ); 
		}
		~Mutex() {
			CloseHandle( mutex );
		}
		void Acquire() {
			WaitForSingleObject( mutex, INFINITE );
		}
		void Release() {
			ReleaseMutex( mutex );
		}
	};

	class Threaingd {
		HANDLE threadId;
	public:
		Threading() : running( false ) {
		}
		bool running;
		void Start();
		virtual void Run() = 0;
	};
#endif	
	
	class ScopedMutex {
		Mutex & m;
	public:
		ScopedMutex( Mutex &inMutex ) : m( inMutex ) {
			m.Acquire();
		}
		~ScopedMutex() {
			m.Release();
		}
	};
	
}

#endif // __STARS_THREADING_H__
