#ifndef __STARS_CONTROL_H__
#define __STARS_CONTROL_H__

#include "stars/initial.h"

#include <string>
#include <deque>

namespace stars {

	class Control {
		std::deque<std::string> outputBuffer;
		std::deque<std::string> history;
		std::string commandLine;
		int cursorPos;
		int historyPos;
		KeyEventHandler previousHandler;
		bool active;
	public:
		Control();
		std::string CommandLine() const {
			return commandLine;
		}
		bool IsActive() const {
			return active;
		}
		void Activate();
		void Deactivate();
		void ProcessKeyEvent( int key, KeyStateEnum keyState );
		void AppendOutput( const std::string & line );
		void Draw();
	};
	
	extern Control control;

}

#endif // __STARS_CONTROL_H__
