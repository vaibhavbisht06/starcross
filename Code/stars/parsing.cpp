#include "stars/lay.h"
#include "stars/parsing.h"
#include <map>
#include <stdlib.h>

using namespace std;

namespace {
	
	bool initialized = false;
	struct OnHeap {
		map< char, char > parenMap;		
	};
	OnHeap *oh = NULL;
		
	void initialize() {
		if ( initialized ) {
			return;
		}
		oh = new OnHeap;
		
		oh->parenMap.clear();
		oh->parenMap['"'] = '"';
		oh->parenMap['('] = ')';
		oh->parenMap['['] = ']';
		oh->parenMap['{'] = '}';
		
		initialized = true;
	}

}

namespace stars {
	
	bool StringToFloat( const string & s, float & val ){
		if ( s.size() == 0 ) {
			return false;
		}
		char * end;
		const char * bgn = s.c_str();
		val = strtod( bgn, &end );
		return ( end - bgn ) == s.size();
	}

	bool IsDelimiter( const char *delimiters, char c ) {
		while( *delimiters != 0 && *delimiters != c ) {
			delimiters++;
		}
		return *delimiters == c;
	}
	
	vector< Token > TokenizeString( const char *str, const char *delimiters ) {
		string s( str );
		vector< Token > toks;
		//Output("Called TokenizeString( %s )\n", str );
		initialize();
		
		while( s.size() > 0 ) {
			while( s.size() > 0 && IsDelimiter( delimiters, s[0] ) ) {
				//Output( "Erasing char: %s\n", s.c_str() );
				s.erase( s.begin() );
			}
			string t;
			vector<char> parenStack;
			bool prevWasBackslash = false;
			bool hadParens = false;
			while( s.size() > 0 && ( prevWasBackslash || ( ! IsDelimiter( delimiters, s[0] ) ) || ( parenStack.size() > 0 ) ) ) {
				bool append = true;
				if ( prevWasBackslash ) {
					prevWasBackslash = false;
				} else if ( s[0] == '\\' ) {
					prevWasBackslash = true;
					append = false;
				} else if ( parenStack.size() > 0 && s[0] == parenStack.back() ) {
					parenStack.pop_back();
					append = s[0] != '"';
				} else if ( oh->parenMap.count( s[0] ) ) {
					parenStack.push_back( oh->parenMap[ s[0] ] );
					append = s[0] != '"';
					hadParens = true;
				}
				if ( append ) {
					t += s[0];
				}
				s.erase( s.begin() );
				//Output( "Moving char: t=%s s=%s\n", t.c_str(), s.c_str()  );
			}
			if ( t.size() > 0 || hadParens ) {
				Token tok;
				tok.valString = t;
				if ( StringToFloat( t, tok.valNumber ) ) {
					tok.type = TokenType_Number;
					//Output( "Adding Number token %d : %f\n", int( toks.size() ), tok.valNumber );
				} else {
					tok.type = TokenType_String;
					//Output( "Adding String token %d : %s\n", int( toks.size() ), tok.valString.c_str() );
				}
				toks.push_back( tok );
			}
		}
		return toks;
	}
}
