
#include "stars/commanding.h"
#include "stars/outcome.h"

using namespace std;
using namespace stars;

namespace {
	
	void ListAtomics( const vector< Token > & tokens ) {
		for ( int i = 0; i < GetAtomicTableSize(); i++ ) {
			Atomic a = GetAtomic( i );
			Output( "Atom %d = %s", i, a.Str().c_str() );
		}
	}
	CommandFunc ListAtomsCmd ( "listatoms", "list the contents of the atom table", ListAtoms );
	
}
